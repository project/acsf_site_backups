<?php

namespace Drupal\acsf_site_backups;

/**
 * ACSF Client.
 */
class AcsfClient {

  /**
   * ACSF Rest API connection.
   */
  public static function acsfRestApiConnect($method, $path, $site_id = 0, $data = []) {
    $params = [];
    $api_url = \Drupal::config('acsf_site_backups.settings')->get('api_endpoint');
    $username = \Drupal::config('acsf_site_backups.settings')->get('username');
    $api_key = \Drupal::config('acsf_site_backups.settings')->get('api_key');

    try {
      $params['auth'] = [$username, $api_key];
      if (!empty($data['form_params'])) {
        $params['form_params'] = $data['form_params'];
      }

      $response = \Drupal::httpClient()->$method($api_url . $path, $params);
      if ($response->getStatusCode() == 200) {
        $data = json_decode($response->getBody()->getContents());
        return $data;
      }
    }
    catch (\Exception $exc) {
      \Drupal::messenger()->addError($exc->getMessage());
    }
    return [];
  }

  /**
   * Create a backup for selected site with selected componenets.
   */
  public static function pingApi() {
    $path = "/ping";
    $data = self::acsfRestApiConnect('GET', $path, 0, []);
    if (!empty($data)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the site list from ACSF.
   */
  public static function getSitesList() {
    $sites = [];
    $sites_count = 0;
    $path = "/sites?limit=100&page=1";
    $sites_list = self::acsfRestApiConnect('GET', $path);
    $sites_count = $sites_list->count;
    $pages_count = ceil($sites_count / 100);
    foreach ($sites_list->sites as $site) {
      $sites[$site->id] = $site->site;
    }

    // Get sites list more than 100.
    if ($sites_count > 1) {
      for ($i = 2; $i <= $pages_count; $i++) {
        $path = "/sites?limit=100&page=$i";
        $sites_list = self::acsfRestApiConnect('GET', $path);
        foreach ($sites_list->sites as $site) {
          $sites[$site->id] = $site->site;
        }
      }
    }
    return $sites;
  }

  /**
   * Create a backup for selected site with selected componenets.
   */
  public static function createSiteBackup($site_id, $site_name, $params) {
    $path = "/sites/$site_id/backup";
    $data = [
      'form_params' => [
        'components' => !(empty($params['components'])) ? $params['components'] : ['database'],
      ],
    ];
    $data = self::acsfRestApiConnect('POST', $path, $site_id, $data);
    return $data;
  }

  /**
   * List all the site specific backups available.
   */
  public static function listSiteBackups($site_id) {
    $path = "/sites/$site_id/backups";
    $data = self::acsfRestApiConnect('GET', $path, $site_id);
    return $data;
  }

  /**
   * Restore the selected backup.
   */
  public static function restoreSiteBackup($site_id, $backup_id) {
    $path = "/sites/$site_id/restore";
    $data = [
      'backup_id' => $backup_id,
      'form_params' => [
        'target_site_id' => $site_id,
        'backup_id' => $backup_id,
      ],
    ];
    $data = self::acsfRestApiConnect('POST', $path, $site_id, $data);
    return $data;
  }

}
