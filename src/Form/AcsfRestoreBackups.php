<?php

namespace Drupal\acsf_site_backups\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\acsf_site_backups\AcsfClient;

/**
 * Restore ACSF site backups.
 */
class AcsfRestoreBackups extends FormBase {

  /**
   * Get form ID.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'acsf_site_backups_restore_backup';
  }

  /**
   * Build form.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check the API connection.
    if (!AcsfClient::pingApi()) {
      $form['help'] = [
        '#markup' => $this->t('Not able to connect to ACSF API. Please validate the settings.'),
      ];
      return $form;
    }

    // Get the sites list from ACSF and prepare the form.
    $sites = AcsfClient::getSitesList();
    $form['help'] = [
      '#markup' => $this->t('Select the below list of sites to restore latest backups from ACSF.'),
    ];
    $form['sites'] = [
      '#title' => $this->t('Select sites'),
      '#type' => 'checkboxes',
      '#options' => $sites,
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Restore latest backups'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Submit form.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_names = $form['sites']['#options'];
    $values = $form_state->getValues();
    $sites = array_keys(array_filter($values['sites']));

    foreach ($sites as $site_id) {
      $backups = AcsfClient::listSiteBackups($site_id);
      if (!empty($backups->backups[0])) {
        // Restore the latest backup.
        $restore = AcsfClient::restoreSiteBackup($site_id, $backups->backups[0]->id);
        if (!empty($restore->task_id)) {
          \Drupal::messenger()->addMessage($this->t('Backup restore request has been sent to ACSF for @name (@id) with task id @taskid.', [
            '@name' => $site_names[$site_id],
            '@id' => $site_id,
            '@taskid' => $restore->task_id,
          ]));
        }
      }
    }
  }

}
