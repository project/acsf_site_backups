<?php

namespace Drupal\acsf_site_backups\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\acsf_site_backups\AcsfClient;

/**
 * Create ACSF site backups.
 */
class AcsfCreateBackups extends FormBase {

  /**
   * Get form ID.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'acsf_site_backups_create_site_backup';
  }

  /**
   * Build form.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormInterface::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check the API connection.
    if (!AcsfClient::pingApi()) {
      $form['help'] = [
        '#markup' => $this->t('Not able to connect to ACSF API. Please validate the settings.'),
      ];
      return $form;
    }

    // Get the sites list from ACSF and prepare the form.
    $sites = AcsfClient::getSitesList();
    $form['help'] = [
      '#markup' => $this->t('Select the below list of sites to create backups in ACSF.'),
    ];
    $form['components'] = [
      '#title' => $this->t('Select components'),
      '#type' => 'checkboxes',
      '#options' => [
        'codebase' => 'codebase',
        'database' => 'database',
        'public files' => 'public files',
        'private files' => 'private files',
        'themes' => 'themes',
      ],
      '#default_value' => ['database'],
      '#required' => TRUE,
    ];
    $form['sites'] = [
      '#title' => $this->t('Select sites'),
      '#type' => 'checkboxes',
      '#options' => $sites,
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create backups'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Submit Form.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormInterface::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_names = $form['sites']['#options'];
    $values = $form_state->getValues();
    $sites = $values['sites'];
    $components = array_keys(array_filter($values['components']));
    foreach ($sites as $id) {
      $params = [];
      if ($id) {
        $params['components'] = $components;
        $data = AcsfClient::createSiteBackup($id, $site_names[$id], $params);
        if ($data->task_id) {
          \Drupal::messenger()->addMessage($this->t('Back up request has been sent to ACSF for @name (@id) with task id @taskid.', [
            '@name' => $site_names[$id],
            '@id' => $id,
            '@taskid' => $data->task_id,
          ]));
        }
      }
    }
  }

}
