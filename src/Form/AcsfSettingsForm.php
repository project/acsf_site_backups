<?php

namespace Drupal\acsf_site_backups\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form.
 */
class AcsfSettingsForm extends ConfigFormBase {

  /**
   * Get configuration name.
   *
   * @return string[]
   *   Returns the configuration name.
   */
  protected function getEditableConfigNames() {
    return ['acsf_site_backups.settings'];
  }

  /**
   * Get form ID.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormInterface::getFormId()
   */
  public function getFormId() {
    return 'acsf_site_backups_admin_settings';
  }

  /**
   * Get build form.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\ConfigFormBase::buildForm()
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('acsf_site_backups.settings');
    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('ACSF API configurations'),
      '#open' => TRUE,
    ];
    $form['api']['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ACSF API end point (v1)'),
      '#default_value' => $config->get('api_endpoint'),
      '#description' => $this->t('Ex: https://www.demo.acquia-cc.com/api/v1'),
    ];
    $form['api']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User name'),
      '#default_value' => $config->get('username'),
    ];
    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $config->get('api_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Validate form.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\FormBase::validateForm()
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    try {
      // Ping the service URL for testing.
      $response = \Drupal::httpClient()->get($values['api_endpoint'] . '/ping', [
        'auth' => [
          $values['username'],
          $values['api_key'],
        ],
      ]);
      if ($response->getStatusCode() != 200) {
        $form_state->setErrorByName('api_endpoint', 'API connection error. Please check the API details.');
      }
    }
    catch (\Exception $exc) {
      $form_state->setErrorByName('api_endpoint', $exc->getMessage());
    }
  }

  /**
   * Form submit handler.
   *
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Form\ConfigFormBase::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form_data = $this->configFactory->getEditable('acsf_site_backups.settings');
    foreach ($values as $key => $value) {
      $form_data->set($key, $value);
    }
    $form_data->save();
    parent::submitForm($form, $form_state);
  }

}
